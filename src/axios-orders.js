import axios from "axios";

const axiosOrders = axios.create({
  baseURL: "https://js9-blog-screw192-default-rtdb.firebaseio.com/"
});

export default axiosOrders;