import React, {useState, useEffect} from 'react';
import axiosOrders from "../../axios-orders";
import moment from "moment";

import withLoaderHandler from "../../hoc/withLoaderHandler/withLoaderHandler";
import Button from "../../components/UI/Button/Button";
import "./FullPost.css";

const FullPost = props => {
  const [fullPostData, setFullPostData] = useState({});

  useEffect(() => {
    const fetchData = async () => {
      const response = await axiosOrders.get("posts/" + props.match.params.id + ".json");
      setFullPostData(response.data);
    };

    fetchData().catch(console.error);
  }, [props.match.params.id]);

  const removePostHandler = async () => {
    await axiosOrders.delete("posts/" + props.match.params.id + ".json");
    props.history.replace("/");
  };

  const editPostHandler = () => {
    props.history.replace(props.match.params.id + "/edit");
  }

  const dateFormatted = moment(fullPostData.datetime).format("D MMMM YYYY, HH:mm");

  return (
      <>
        <div className="FullPostBlock">
          <div className="container_sm">
            <div className="FullPost">
              <h3 className="FullPostTitle">{fullPostData.postTitle}</h3>
              <p className="FullPostCreatedDate">{dateFormatted}</p>
              <p className="FullPostText">{fullPostData.postDescription}</p>
              <div className="FullPostButtonBlock">
                <Button type="grey" label="Edit" onClick={editPostHandler}/>
                <Button type="red" label="Remove" onClick={removePostHandler}/>
              </div>
            </div>
          </div>
        </div>
      </>
  );
};

export default withLoaderHandler(FullPost, axiosOrders);