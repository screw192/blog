import React, {useState, useEffect} from 'react';
import axiosOrders from "../../axios-orders";

import withLoaderHandler from "../../hoc/withLoaderHandler/withLoaderHandler";
import Post from "./Post/Post";
import "./Home.css";

const Home = props => {
  const [postsData, setPostsData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const response = await axiosOrders.get("posts.json");

      let responseArray = [];
      for (let key in response.data) {
        responseArray.unshift({
          ...response.data[key],
          id: key
        });
      }
      setPostsData(responseArray);
    };

    fetchData().catch(console.error);
  }, []);

  const posts = postsData.map(post => {
    return (
        <Post
            key={post.id}
            id={post.id}
            date={post.datetime}
            title={post.postTitle}
            {...props}
        />
    );
  });

  return (
      <>
        <div className="HomeBlock">
          <div className="Home container_sm">
            {posts}
          </div>
        </div>
      </>
  );
};

export default withLoaderHandler(Home, axiosOrders);