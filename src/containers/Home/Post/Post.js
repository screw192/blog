import React from 'react';
import moment from 'moment';

import Button from "../../../components/UI/Button/Button";
import "./Post.css";

const Post = props => {
  const readMoreHandler = () => {
    props.history.push("/posts/" + props.id);
  };

  const date = moment(props.date).format("D MMMM YYYY, HH:mm");

  return (
      <div id={props.id} className="PostBody">
        <div className="PostInfo">{date}</div>
        <h3 className="PostTitle">{props.title}</h3>
        <Button type="grey" label="Read more..." onClick={readMoreHandler} />
      </div>
  );
};

export default Post;