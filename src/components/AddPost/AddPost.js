import React, {useState, useEffect} from 'react';
import axiosOrders from "../../axios-orders";

import withLoaderHandler from "../../hoc/withLoaderHandler/withLoaderHandler";
import Button from "../UI/Button/Button";
import "./AddPost.css";

const AddPost = props => {
  const [post, setPost] = useState({
    postTitle: "",
    postDescription: ""
  });

  useEffect(() => {
    if (props.match.params.id) {
      const fetchData = async () => {
        const response = await axiosOrders.get("posts/" + props.match.params.id + ".json");
        const postToEdit = {
          postTitle: response.data.postTitle,
          postDescription: response.data.postDescription
        };

        setPost(postToEdit);
      };

      fetchData().catch(console.error);
    } else {
      setPost({
        postTitle: "",
        postDescription: ""
      })
    }
  }, [props.match.params.id]);

  const postInputChanged = event => {
    const {name, value} = event.target;

    setPost(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  const submitHandler = async event => {
    event.preventDefault();

    const date = Date.now();
    const postData = {
      ...post,
      datetime: date,
    };

    try {
      if (props.match.params.id) {
        await axiosOrders.patch("posts/" + props.match.params.id + ".json", post);
      } else {
        await axiosOrders.post("/posts.json", postData);
      }
    } finally {
      props.history.push("/");
    }
  };

  return (
      <>
        <div className="AddPostBlock">
          <div className="container_sm">
            <div className="AddPost">
              <h4 className="BlockTitle">{props.title}</h4>
              <form onSubmit={submitHandler}>
                <div className="FormRow">
                  <label htmlFor="postTitle">Title</label>
                  <input
                      id="postTitle"
                      type="text" name="postTitle"
                      value={post.postTitle}
                      onChange={postInputChanged}
                  />
                </div>
                <div className="FormRow">
                  <label htmlFor="postDescription">Description</label>
                  <textarea
                      id="postDescription"
                      name="postDescription"
                      value={post.postDescription}
                      onChange={postInputChanged}
                      style={{resize: "none"}}
                  />
                </div>
                <Button type="blue" label="Save" />
              </form>
            </div>
          </div>
        </div>
      </>
  );
};

export default withLoaderHandler(AddPost, axiosOrders);