import React from 'react';

import "./Contacts.css";

const Contacts = () => {
  return (
      <div className="ContactsBlock">
        <div className="container_sm">
          <div className="Contacts">
            <h4>Address:</h4>
            <p>2983 Jadewood Farms</p>
            <p>BEDMINSTER, PA 18910</p>
            <h4>Phone:</h4>
            <p>973-349-3502</p>
          </div>
        </div>
      </div>
  );
};

export default Contacts;