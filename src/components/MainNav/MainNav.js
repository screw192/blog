import React from 'react';
import {Link, NavLink} from "react-router-dom";

import "./MainNav.css";

const MainNav = () => {
  return (
      <div className="MainNavBlock">
        <div className="container">
          <div className="MainNav">
            <Link to="/" className="Logo">Blog</Link>
            <nav className="NavMenu">
              <NavLink className="NavMenuItem" exact to="/">Home</NavLink>
              <NavLink className="NavMenuItem" to="/posts/add">Add</NavLink>
              <NavLink className="NavMenuItem" to="/about">About</NavLink>
              <NavLink className="NavMenuItem" to="/contacts">Contacts</NavLink>
            </nav>
          </div>
        </div>
      </div>
  );
};

export default MainNav;