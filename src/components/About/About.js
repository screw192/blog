import React from 'react';

import "./About.css";

const About = () => {
  return (
      <div className="AboutBlock">
        <div className="container_sm">
          <div className="About">
            <h4>What we're doing:</h4>
            <p>Lorem ipsum dolor sit amet, Jopa Govno consectetur adipisicing elit. Aliquam doloribus eligendi expedita! Nobis,
              officia, perspiciatis. </p>
            <p>Alias aliquam aliquid architecto beatae cupiditate dicta dolorum earum eligendi eum
              facilis id in iure labore minima mollitia nemo neque numquam porro possimus quas, quidem quo quod sit
              tenetur veniam vitae voluptate. A asperiores nostrum quasi, rerum similique ullam voluptate.
            </p>
            <p>Aspernatur
              atque consequuntur, dicta doloribus dolorum earum et fugiat illo laboriosam libero molestiae molestias nam
              necessitatibus, nihil non numquam obcaecati omnis perferendis quam quasi quibusdam quidem quos soluta
              tempore voluptates! Accusantium blanditiis cum dignissimos eaque et illo molestias nobis perferendis quasi
              totam, unde, ut voluptate!
            </p>
          </div>
        </div>
      </div>
  );
};

export default About;