import React from 'react';

import "./Button.css";

const Button = ({type, label, onClick}) => {
  const classArray = ["Button", "Button_" + type]

  return (
      <button
          className={classArray.join(" ")}
          onClick={onClick}
      >
        {label}
      </button>
  );
};

export default Button;