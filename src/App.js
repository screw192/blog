import React from "react";
import {Route, Switch} from "react-router-dom";

import MainNav from "./components/MainNav/MainNav";
import Home from "./containers/Home/Home";
import AddPost from "./components/AddPost/AddPost";
import FullPost from "./containers/FullPost/FullPost";
import About from "./components/About/About";
import Contacts from "./components/Contacts/Contacts";
import './App.css';

const App = () => {
  return (
      <div className="App">
        <MainNav />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/posts/add"
              render={props => (
                  <AddPost title="Add new post" {...props}/>
              )}
          />
          <Route exact path="/posts/:id" component={FullPost} />
          <Route path="/posts/:id/edit"
              render={props => (
                  <AddPost title="Edit post" {...props}/>
              )}
          />
          <Route path="/about" component={About} />
          <Route path="/contacts" component={Contacts} />
        </Switch>
      </div>
  );
};

export default App;