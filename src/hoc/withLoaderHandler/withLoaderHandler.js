import React, {useState, useEffect, useMemo} from 'react';
import Preloader from "../../components/UI/Preloader/Preloader";

const withLoaderHandler = (WrappedComponent, axios) => {
  return function WithLoaderHandler(props) {
    const [loading, setLoading] = useState(false);

    const reqIc = useMemo(() => {
      axios.interceptors.request.use(request => {
        setLoading(true);
        return request;
      });
    }, []);

    const resIc = useMemo(() => {
      axios.interceptors.response.use(response => {
        setLoading(false);
        return response;
      }, error => {
        throw error;
      });
    }, []);

    useEffect(() => {
      return () => {
        axios.interceptors.response.eject(reqIc);
        axios.interceptors.response.eject(resIc);
      }
    }, [reqIc, resIc]);

    let preloader;
    loading ? preloader = <Preloader/> : preloader = null;

    return (
        <>
          {preloader}
          <WrappedComponent {...props} />
        </>
    );
  }
};

export default withLoaderHandler;